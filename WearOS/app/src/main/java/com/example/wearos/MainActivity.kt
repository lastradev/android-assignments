package com.example.wearos

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.wearos.databinding.ActivityRelojBinding

class MainActivity : Activity(), AdapterView.OnItemSelectedListener {

    private lateinit var binding: ActivityRelojBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRelojBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvSaludo.text = "Hola amiguito"
        binding.bnCambio.text = "Enviar"

        val adaptador = ArrayAdapter.createFromResource(this, R.array.misOpciones, android.R.layout.simple_spinner_item)

        binding.spOpciones.adapter = adaptador
        binding.spOpciones.onItemSelectedListener = this

        binding.bnCambio.setOnClickListener{
            var alerta = AlertDialog.Builder(this)
            alerta.setTitle("Atencion")
                .setMessage("Quiere enviar el saludo $textoSeleccionado")
                .setCancelable(false)
                .setPositiveButton("OK",
                    {dialogInterface, i ->
                        binding.tvSaludo.text = textoSeleccionado
                    })
        }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}
