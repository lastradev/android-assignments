package com.example.dogsapi.api

import com.example.dogsapi.models.DogImage
import com.example.dogsapi.models.DogImages
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("breeds/image/random")
    fun getRandomDogImage(): Call<DogImage>

    @GET("breed/{breed}/images")
    fun getDogImagesByBreed(@Path("breed") breed: String): Call<DogImages>
}