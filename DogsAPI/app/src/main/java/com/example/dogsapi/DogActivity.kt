package com.example.dogsapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.example.dogsapi.api.API
import com.example.dogsapi.models.DogImage
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DogActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dog)

        val ivDog = findViewById<ImageView>(R.id.ivDog)

        val apiCall = API().createAPIService()

        apiCall.getRandomDogImage().enqueue(object: Callback<DogImage> {
            override fun onResponse(call: Call<DogImage>, response: Response<DogImage>) {
                Picasso.get().load(response.body()?.message.toString()).into(ivDog)
            }

            override fun onFailure(call: Call<DogImage>, t: Throwable) {
                Toast.makeText(this@DogActivity, "Can't connect to API", Toast.LENGTH_SHORT).show()
            }
        })
    }
}