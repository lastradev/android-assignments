package com.example.dogsapi

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btDog = findViewById<Button>(R.id.btDog)
        val btDogs = findViewById<Button>(R.id.btDogs)

        btDog.setOnClickListener {
            val i = Intent(this, DogActivity::class.java)
            startActivity(i)
        }

        btDogs.setOnClickListener {
            val i = Intent(this, DogsActivity::class.java)
            startActivity(i)
        }
    }
}