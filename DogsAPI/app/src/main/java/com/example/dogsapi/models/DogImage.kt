package com.example.dogsapi.models

data class DogImage(
    val status: String,
    val message: String
)
