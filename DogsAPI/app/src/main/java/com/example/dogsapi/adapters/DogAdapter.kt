package com.example.dogsapi.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.dogsapi.R
import com.squareup.picasso.Picasso

class DogAdapter(dogs: List<String>, context: Context) : RecyclerView.Adapter<DogAdapter.ViewContainer> () {
    private var innerDogs: List<String> = dogs
    var innerContext: Context = context

    inner class ViewContainer(view: View) :
        RecyclerView.ViewHolder(view){

        var ivBreedDog: ImageView

        init {
            ivBreedDog = view.findViewById(R.id.ivBreedDog)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewContainer {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.dog_layout, parent, false)

        return ViewContainer(view)
    }

    override fun getItemCount(): Int {
        return innerDogs.size
    }

    override fun onBindViewHolder(holder: ViewContainer, position: Int) {
        val dog: String = innerDogs[position]

        Picasso.get().load(dog).into(holder.ivBreedDog)
    }

}
