package com.example.dogsapi.models

data class DogImages(
    val status: String,
    val message: List<String>
)
