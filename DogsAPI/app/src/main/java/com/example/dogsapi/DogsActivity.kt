package com.example.dogsapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dogsapi.adapters.DogAdapter
import com.example.dogsapi.api.API
import com.example.dogsapi.models.DogImages
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DogsActivity : AppCompatActivity() {
    lateinit var rvDogs: RecyclerView
    lateinit var dogs: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dogs)

        rvDogs = findViewById(R.id.recycler_view)
        dogs = emptyList()

        val btBreed = findViewById<Button>(R.id.btBreed)
        btBreed.setOnClickListener { fetchDogs() }
    }

    private fun fetchDogs(){
        val apiCall = API().createAPIService()

        apiCall.getDogImagesByBreed("hound").enqueue(object: Callback<DogImages> {
            override fun onResponse(call: Call<DogImages>, response: Response<DogImages>) {
                dogs = response.body()?.message!!
            }

            override fun onFailure(call: Call<DogImages>, t: Throwable) {
                Toast.makeText(this@DogsActivity, "Can't connect to API", Toast.LENGTH_SHORT).show()
            }
        })

        val layoutAdministrator = LinearLayoutManager(this@DogsActivity, LinearLayoutManager.VERTICAL, false)
        rvDogs.adapter = DogAdapter(dogs, this@DogsActivity)
        rvDogs.layoutManager = layoutAdministrator
    }
}