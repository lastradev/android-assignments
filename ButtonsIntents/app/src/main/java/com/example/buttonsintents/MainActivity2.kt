package com.example.buttonsintents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val grade = intent.getStringExtra("grade")?.toIntOrNull()
        val tvResult = findViewById<TextView>(R.id.tvResult)
        val tvSubtitle = findViewById<TextView>(R.id.tvSubtitle)
        val btBack = findViewById<Button>(R.id.btBack)

        if (grade != null) {
            if (grade < 7) {
                tvResult.text = "Obtuviste un $grade :("
                tvSubtitle.text = "Sigue participando"
            } else {
                tvResult.text = "Felicidades, aprobaste la materia con $grade"
            }
        }

        btBack.setOnClickListener {
            finish()
        }
    }
}