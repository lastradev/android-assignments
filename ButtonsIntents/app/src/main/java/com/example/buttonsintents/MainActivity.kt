package com.example.buttonsintents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val etGrade = findViewById<EditText>(R.id.etGrade)
        val btSend = findViewById<Button>(R.id.btSend)

        btSend.setOnClickListener {
            val grade = etGrade.text.toString()
            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("grade", grade)
            startActivity(intent)
        }
    }
}