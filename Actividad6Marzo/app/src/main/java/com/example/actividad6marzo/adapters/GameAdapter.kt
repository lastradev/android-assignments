package com.example.actividad6marzo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.actividad6marzo.AGE_KEY
import com.example.actividad6marzo.R
import com.example.actividad6marzo.models.Game

class GameAdapter(games: ArrayList<Game>, context: Context) : RecyclerView.Adapter<GameAdapter.ViewContainer> () {
    private var innerGames: ArrayList<Game> = games
    var innerContext: Context = context

    inner class ViewContainer(view: View) :
        RecyclerView.ViewHolder(view){

        var tvGameName : TextView
        var tvPrice : TextView
        var tvConsole : TextView
        var btBuy : Button
        var tvClassification: TextView

        init {
            tvGameName = view.findViewById(R.id.tvGameName)
            tvPrice = view.findViewById(R.id.tvPrice)
            tvConsole = view.findViewById(R.id.tvConsole)
            btBuy = view.findViewById(R.id.btBuy)
            tvClassification = view.findViewById(R.id.tvClassification)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewContainer {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.game_layout, parent, false)

        return ViewContainer(view)
    }

    override fun onBindViewHolder(holder: ViewContainer, position: Int) {
        val game: Game = innerGames[position]
        holder.tvGameName.text = game.name
        holder.tvConsole.text = game.console
        holder.tvPrice.text = "$${game.price} MXN"
        holder.tvClassification.text = "Clasificación: ${game.classification}"

        holder.btBuy.setOnClickListener {
            val age = getAge()
            val classification = game.classification

            if (classification == "R" && age < 18) {
                showProhibitBuyToast()
            } else if(classification == "T" && age < 5 ){
                showProhibitBuyToast()
            } else {
                showSuccessfulBuyToast()
            }
        }
    }

    private fun getAge(): Int {
        val infoPreferences = innerContext.getSharedPreferences("MAIN_FORM", AppCompatActivity.MODE_PRIVATE)
        return infoPreferences.getInt(AGE_KEY, 0)
    }

    private fun showProhibitBuyToast() {
        showToast("No tienes la edad para comprar esto.")
    }

    private fun showSuccessfulBuyToast() {
        showToast("¡Tienes la edad suficiente para comprarlo! (o has mentido correctamente)")
    }

    private fun showToast(message: String) {
        Toast.makeText(innerContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun getItemCount(): Int {
        return innerGames.size
    }
}