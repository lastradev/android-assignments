package com.example.actividad6marzo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.android.material.switchmaterial.SwitchMaterial

const val NAME_KEY = "name"
const val AGE_KEY = "age"
const val HEIGHT_KEY = "height"
const val MONEY_KEY = "money"

class MainActivity : AppCompatActivity() {
    private lateinit var etName: EditText
    private lateinit var etAge: EditText
    private lateinit var etHeight: EditText
    private lateinit var etMoney: EditText
    private lateinit var btSave: Button
    private lateinit var swPreferences: SwitchMaterial

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etName = findViewById(R.id.etName)
        etAge = findViewById(R.id.etAge)
        etHeight = findViewById(R.id.etHeight)
        etMoney = findViewById(R.id.etMoney)

        btSave = findViewById(R.id.btSave)
        swPreferences = findViewById(R.id.swPreferences)

        btSave.setOnClickListener {
            savePreferences()
            navigateToShop()
        }
    }

    private fun savePreferences() {
        if(!swPreferences.isChecked) return

        val name = etName.text.toString()
        val age = etAge.text.toString().toInt()
        val height = etHeight.text.toString().toFloat()
        val money = etMoney.text.toString().toFloat()

        val formPreferences = getSharedPreferences("MAIN_FORM", MODE_PRIVATE)
        val editor = formPreferences.edit()

        editor.putString(NAME_KEY, name)
        editor.putInt(AGE_KEY, age)
        editor.putFloat(HEIGHT_KEY, height)
        editor.putFloat(MONEY_KEY, money)
        editor.apply()
    }

    private fun navigateToShop() {
        val i = Intent(this, MainActivity2::class.java)
        startActivity(i)
    }
}