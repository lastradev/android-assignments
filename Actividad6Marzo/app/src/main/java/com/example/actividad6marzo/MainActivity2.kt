package com.example.actividad6marzo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.actividad6marzo.adapters.GameAdapter
import com.example.actividad6marzo.models.Game
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity2 : AppCompatActivity() {
    private lateinit var rvGames : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        rvGames = findViewById(R.id.recycler_view)
        val fabInfo: FloatingActionButton = findViewById(R.id.fabInfo)
        val btGrid2: Button = findViewById(R.id.btGrid2)
        val btGrid3: Button = findViewById(R.id.btGrid3)
        val btList: Button = findViewById(R.id.btList)
        val btSided: Button = findViewById(R.id.btSided)

        val games = arrayListOf(
               Game(1, "Mario Bros.", 800.0f, "Nintendo", R.drawable.mario, "E+"),
               Game(2, "Mario Bros. 2", 1300.0f, "Nintendo", R.drawable.mario, "T"),
               Game(2, "Mario Bros. 3", 1800.0f, "Nintendo", R.drawable.mario, "R")
        )

        showLinearView(LinearLayoutManager.VERTICAL)
        rvGames.adapter = GameAdapter(games, this)

        fabInfo.setOnClickListener { showInfo() }
        btGrid2.setOnClickListener { showGridView(columns = 2) }
        btGrid3.setOnClickListener { showGridView(columns = 3) }
        btList.setOnClickListener { showLinearView(LinearLayoutManager.VERTICAL) }
        btSided.setOnClickListener { showLinearView(LinearLayoutManager.HORIZONTAL) }
    }

    private fun showGridView(columns: Int){
        val layoutAdministrator = GridLayoutManager(this, columns)
        rvGames.layoutManager = layoutAdministrator
    }

    private fun showLinearView(direction: Int){
        val layoutAdministrator = LinearLayoutManager(this, direction, false)
        rvGames.layoutManager = layoutAdministrator
    }

    private fun showInfo() {
        val infoPreferences = getSharedPreferences("MAIN_FORM", MODE_PRIVATE)

        val name = infoPreferences.getString(NAME_KEY, "missing name")
        val age = infoPreferences.getInt(AGE_KEY, 0)
        val height = infoPreferences.getFloat(HEIGHT_KEY, 0f)
        val money = infoPreferences.getFloat(MONEY_KEY, 0f)

        val infoText = "Nombre: $name, edad: $age, altura: $height, dinero: $money"

        Toast.makeText(this, infoText, Toast.LENGTH_SHORT).show()
    }
}