package com.example.botones

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var tvFrase: EditText
        tvFrase = findViewById(R.id.editTextFrase)

        var etFrase: EditText
        etFrase = findViewById(R.id.editTextFrase)

        var bnMostrar = findViewById<Button>(R.id.buttonShow)
        bnMostrar.setOnClickListener {
            val FRASE = etFrase.text.toString()

            Snackbar.make(tvFrase, "Su frase fue $FRASE", Snackbar.LENGTH_SHORT).show()
        }

        var bnSalir = findViewById<Button>(R.id.bnCerrar)
        bnSalir.setOnClickListener {
            finish()
        }

    }
}