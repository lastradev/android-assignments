package com.example.botones

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity2 : AppCompatActivity() {
    private var cambioIcon: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val bnOpenScreen = findViewById<Button>(R.id.bnScreen)
        bnOpenScreen.setOnClickListener{
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }

        val ivLogo = findViewById<ImageView>(R.id.ivLogo)
        ivLogo.setOnClickListener {
            var arregloImagenes = arrayOf<Int>(R.drawable.iest, R.drawable.ic_launcher_background)

            if (cambioIcon) {
                ivLogo.setImageResource(arregloImagenes[0])
            }  else {
                ivLogo.setImageResource(arregloImagenes[1])
            }

            cambioIcon = !cambioIcon
        }

        val bnCloseScreen = findViewById<Button>(R.id.button2)
        bnCloseScreen.setOnClickListener {
            finish()
        }
    }
}