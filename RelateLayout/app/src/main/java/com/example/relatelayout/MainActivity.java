package com.example.relatelayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

private ImageView ivGatito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bnCanal = findViewById(R.id.bnCanal);
        EditText etCanal = findViewById(R.id.etCanal);
        TextView tvCanal = findViewById(R.id.textView2);
        Spinner spProgramas = findViewById(R.id.spProgramas);
        ivGatito = findViewById(R.id.imageView);

        bnCanal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String texto = etCanal.getText().toString();
                Toast.makeText(MainActivity.this, "El valor era " + texto, Toast.LENGTH_LONG).show();
                tvCanal.setText(texto);
            }
        });

        spProgramas.setOnItemSelectedListener(this);

        ivGatito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, DatosActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String programa = adapterView.getItemAtPosition(i).toString();
        Snackbar.make(adapterView, "Programa seleccionado " + programa, Snackbar.LENGTH_LONG).show();

        if(i == 0) {
            ivGatito.setImageResource(R.drawable.gato1);
        }else if (i == 1) {
            ivGatito.setImageResource(R.drawable.gato2);
        }else{
            ivGatito.setImageResource(R.drawable.gato3);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}