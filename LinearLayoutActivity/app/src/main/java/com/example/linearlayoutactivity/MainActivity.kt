package com.example.linearlayoutactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var bnSuma: Button? = null
    private var bnResta: Button? = null
    private var etNumeroPrimero: Button? = null
    private var etNumeroSegundo: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inicializarVistas()
        asignarEventos()
    }

    private fun asignarEventos() {
        bnSuma?.setOnClickListener(this)
        bnResta?.setOnClickListener(this)
    }

    private fun inicializarVistas() {
        bnSuma = findViewById(R.id.bnSuma)
        bnResta = findViewById(R.id.bnResta)
    }

    override fun onClick(miVista: View?) {
        val numeroUno = etNumeroPrimero?.text.toString().toFloatOrNull()
        val numeroDos = etNumeroSegundo?.text.toString().toFloatOrNull()

        if(numeroUno == null || numeroDos == null) return

        val aritmetica = Aritmetica()

        when(miVista?.id){
            R.id.bnSuma -> {
                val suma = aritmetica.suma(numeroUno, numeroDos)
                val mensaje = Mensajes("La sumatoria fue $suma", this)

                mensaje.mostrarToast()
            }
            R.id.bnResta -> {
                val resta = aritmetica.resta(numeroUno, numeroDos)
                val mensaje = Mensajes("El resultado es $resta", this)

                mensaje.mostrarSnackbar(miVista)
            }
        }
    }
}