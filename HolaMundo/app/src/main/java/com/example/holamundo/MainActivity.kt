package com.example.holamundo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
   private var tvGreeting: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvGreeting = findViewById(R.id.tvGreeting)

        changeGreeting()
        changeGreeting(1)
    }

    private fun changeGreeting() {
        val originalMessage: String = tvGreeting?.text.toString()
        Toast.makeText(this, "Your original message was: $originalMessage", Toast.LENGTH_LONG)
            .show()
    }

    private fun changeGreeting(type: Int) {
        if(type == 1) {
            tvGreeting!!.text = getString(R.string.farewell)
        } else {
            Toast.makeText(this, "Type is $type", Toast.LENGTH_SHORT).show()
        }
    }
}

























